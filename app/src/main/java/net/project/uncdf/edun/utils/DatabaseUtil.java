package net.project.uncdf.edun.utils;

import android.content.Context;
import android.database.SQLException;
import android.os.AsyncTask;

import net.project.uncdf.edun.databaseacces.dao.DaoSession;
import net.project.uncdf.edun.databaseacces.dao.PartenaireDao;
import net.project.uncdf.edun.databaseacces.dao.ThemeDao;
import net.project.uncdf.edun.databaseacces.dao.VideoDao;
import net.project.uncdf.edun.databaseacces.entities.Partenaire;
import net.project.uncdf.edun.databaseacces.entities.Theme;
import net.project.uncdf.edun.databaseacces.entities.Video;

import java.util.ArrayList;
import java.util.List;


public class DatabaseUtil {

    private PartenaireDao partenaireDao;
    private ThemeDao themeDao;
    private VideoDao videoDao;


    public DatabaseUtil(Context context){

        DaoSession daoSession = ConnexionUtil.dbConnexion(context);
         partenaireDao = daoSession.getPartenaireDao();
          themeDao = daoSession.getThemeDao();
         videoDao = daoSession.getVideoDao();

     //   populateDataBase();
    }

/*
    public void populateDataBase(){

       populatePartenaire();
       populateTheme();
       populateVideo();


    }


    public void populatePartenaire(){
        try {
            partenaireDao.deleteAll();
            if(partenaireDao.loadAll().isEmpty()) {
                partenaireDao.insert(new Partenaire(1L,"UNCDF","Le Fonds d’équipement des Nations unies contribue à l'accomplissement des objectifs du Millénaire pour le développement dans les pays les moins avancés. "));
                partenaireDao.insert(new Partenaire(2L,"TreeAid","Traduit de l'anglais-TREE AID est une organisation de développement internationale qui vise à libérer le potentiel des arbres pour réduire la pauvreté et protéger l'environnement en Afrique."));
                partenaireDao.insert(new Partenaire(3L,"UNFPA","Le Fonds des Nations unies pour la population, créé en 1967, est la plus grande source des fonds de développement international pour la population."));
                partenaireDao.insert(new Partenaire(4L,"PARI",""));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void populateTheme(){
        try {
            themeDao.deleteAll();
            if(themeDao.loadAll().isEmpty()) {
                themeDao.insert(new Theme(1L,"Epargne",1L));
                themeDao.insert(new Theme(2L,"Services financiers",1L));
                themeDao.insert(new Theme(3L,"Négociation financière",1L));
                themeDao.insert(new Theme(4L,"Gestion des dettes",1L));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void populateVideo(){
        try {
            videoDao.deleteAll();
            if(videoDao.loadAll().isEmpty()) {
                videoDao.insert(new Video(1L,"CeRGyXdbpWI","","Description","","Titre 1",1L));
                videoDao.insert(new Video(2L,"mzDV4emAfvw","","Description","","Titre 2",1L));
                videoDao.insert(new Video(3L,"CeRGyXdbpWI","","Description","","Titre 3",1L));
                videoDao.insert(new Video(4L,"mzDV4emAfvw","","Description","","Titre 4",1L));
                videoDao.insert(new Video(5L,"CeRGyXdbpWI","","Description","","Titre 5",1L));
                videoDao.insert(new Video(6L,"mzDV4emAfvw","","Description","","Titre 6",1L));
                videoDao.insert(new Video(7L,"CeRGyXdbpWI","","Description","","Titre 7",1L));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



*/

}
