package net.project.uncdf.edun.activity;


import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.databaseacces.entities.Video;
import net.project.uncdf.edun.utils.ConnexionUtil;
import net.project.uncdf.edun.utils.Constantes;
import net.project.uncdf.edun.utils.FileUtils;
import net.project.uncdf.edun.wsclient.WebserviceClient;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cz.msebera.android.httpclient.Header;

import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.videoDao;

public class VideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {


    private YouTubePlayer YPlayer;
    public static final String YoutubeDeveloperKey = "AIzaSyBqEn0g1InyZOXGYm7eprGgxkAW-svJlGg";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private Video video;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        //toolbar = findViewById(R.id.tool_bar);

        //setSupportActionBar(toolbar);

        ConnexionUtil.dbConnexion(this);

        Long idvideo= getIntent().getLongExtra("id_video",0L);
        video = videoDao.load(idvideo);

        YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubeView.initialize(YoutubeDeveloperKey, this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.video_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.action_telecharger:
                downloadVideo(video.getId()+"");
                break;
        }
        return true;
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "There was an error initializing the YouTubePlayer",
                    errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(YoutubeDeveloperKey, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {
        YPlayer = player;
        /*
         * Now that this variable YPlayer is global you can access it
         * throughout the activity, and perform all the player actions like
         * play, pause and seeking to a position by code.
         */
        if (!wasRestored) {
            YPlayer.cueVideo(video.getChemeinvideo());
        }
    }


    public void downloadVideo(final String idVideo){


                WebserviceClient webserviceClient = new WebserviceClient();

                webserviceClient.getClient().get(Constantes.serveur.adresseServices+"/video?id="+idVideo+"", new FileAsyncHttpResponseHandler(this) {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                        System.err.println("FILE DWLOAD ERR");

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, File file) {

                        System.err.println("FILE DWLOAD");
                        System.err.println("FILE DWLOAD "+file.getName());

                        String videoname = file.getName() + ".mp4";
                        String dest = FileUtils.getAppPath(getApplicationContext()) + videoname;


                        if (new File(dest).exists()) {
                            new File(dest).delete();
                        }

                        FileUtils.copy(file,new File(dest));


                    }
                });




    }

}
