package net.project.uncdf.edun.wsclient;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.SyncHttpClient;

/**
 * Created by ASSION on 26/06/2017.
 */
public class WebserviceClient {

    private static AsyncHttpClient client = new AsyncHttpClient();
    private static SyncHttpClient client2 = new SyncHttpClient();

   public AsyncHttpClient getClient(){
       client.setTimeout(30000);
       return  client;
   }
    public SyncHttpClient getSyncClient(){
        client2.setTimeout(30000);
        return  client2;
    }

}
