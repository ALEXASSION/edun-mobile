package net.project.uncdf.edun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.databaseacces.entities.Theme;

import java.util.List;

public class ThemeListFiltreAdapter extends BaseAdapter implements ListAdapter {

    List<Theme> themes;
    LayoutInflater myInflater;

    public ThemeListFiltreAdapter(Context context, List<Theme> themes) {
        this.themes = themes;
        this.myInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return themes.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return themes.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public static class viewCarnetHolder {
        TextView tx_nom;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ThemeListFiltreAdapter.viewCarnetHolder holder;
        if (convertView==null) {
            convertView = myInflater.inflate(R.layout.item_theme_filtre, null);
            holder = new ThemeListFiltreAdapter.viewCarnetHolder();
            holder.tx_nom = (TextView)convertView.findViewById(R.id.tx_nom_theme);


            convertView.setTag(holder);
        } else {
            holder = (ThemeListFiltreAdapter.viewCarnetHolder)convertView.getTag();
        }
        holder.tx_nom.setText(themes.get(position).getNomtheme());


        return convertView;
    }

}
