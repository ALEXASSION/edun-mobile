package net.project.uncdf.edun.utils;

import android.os.Environment;


/**
 * Created by ASSION on 11/02/2016.
 */
public final class Constantes {


    private Constantes(){

    }

    public static class serveur{

      public static final String adresseServices ="https://www.edfin.org/rs/mobile-api";

    }

    public static class dossier{
        public static final String dossierVideo = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Edun/videos/";
    }


}
