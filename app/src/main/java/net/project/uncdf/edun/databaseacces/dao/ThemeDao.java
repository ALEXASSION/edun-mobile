package net.project.uncdf.edun.databaseacces.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import net.project.uncdf.edun.databaseacces.entities.Theme;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table THEME.
*/
public class ThemeDao extends AbstractDao<Theme, Long> {

    public static final String TABLENAME = "THEME";

    /**
     * Properties of entity Theme.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property Idtheme = new Property(1, String.class, "idtheme", false, "IDTHEME");
        public final static Property Nomtheme = new Property(2, String.class, "nomtheme", false, "NOMTHEME");
        public final static Property IdPartenaire = new Property(3, Long.class, "idPartenaire", false, "ID_PARTENAIRE");
    };


    public ThemeDao(DaoConfig config) {
        super(config);
    }
    
    public ThemeDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'THEME' (" + //
                "'_id' INTEGER PRIMARY KEY ," + // 0: id
                "'IDTHEME' TEXT," + // 1: idtheme
                "'NOMTHEME' TEXT," + // 2: nomtheme
                "'ID_PARTENAIRE' INTEGER);"); // 3: idPartenaire
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'THEME'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Theme entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String idtheme = entity.getIdtheme();
        if (idtheme != null) {
            stmt.bindString(2, idtheme);
        }
 
        String nomtheme = entity.getNomtheme();
        if (nomtheme != null) {
            stmt.bindString(3, nomtheme);
        }
 
        Long idPartenaire = entity.getIdPartenaire();
        if (idPartenaire != null) {
            stmt.bindLong(4, idPartenaire);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Theme readEntity(Cursor cursor, int offset) {
        Theme entity = new Theme( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // idtheme
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // nomtheme
            cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3) // idPartenaire
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Theme entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setIdtheme(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setNomtheme(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setIdPartenaire(cursor.isNull(offset + 3) ? null : cursor.getLong(offset + 3));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(Theme entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(Theme entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
