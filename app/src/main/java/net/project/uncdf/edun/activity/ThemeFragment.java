package net.project.uncdf.edun.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.adapter.PartenaireListAdapter;
import net.project.uncdf.edun.adapter.ThemeListAdapter;
import net.project.uncdf.edun.databaseacces.entities.Partenaire;
import net.project.uncdf.edun.databaseacces.entities.Theme;
import net.project.uncdf.edun.utils.ConnexionUtil;
import net.project.uncdf.edun.utils.MyListView;

import java.util.List;

import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.partenaireDao;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.themeDao;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThemeFragment extends Fragment {

    List<Theme> themeList;
    View view;
    ListView listView;

    public ThemeFragment() {
        // Required empty public constructor
    }

    public static ThemeFragment newInstance() {
        ThemeFragment fragment = new ThemeFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConnexionUtil.dbConnexion(getContext());

        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_theme, container, false);
        dataBindListTheme();

        return view;
    }

    public void dataBindListTheme(){
        try {
            themeList = themeDao.loadAll();
            ThemeListAdapter adapter = new ThemeListAdapter(getContext(),themeList);
            listView = view.findViewById(R.id.list_data);
            listView.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
