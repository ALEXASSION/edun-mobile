package net.project.uncdf.edun.databaseacces.dao;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import net.project.uncdf.edun.databaseacces.entities.Parametre;
import net.project.uncdf.edun.databaseacces.entities.Partenaire;
import net.project.uncdf.edun.databaseacces.entities.Theme;
import net.project.uncdf.edun.databaseacces.entities.Video;

import net.project.uncdf.edun.databaseacces.dao.ParametreDao;
import net.project.uncdf.edun.databaseacces.dao.PartenaireDao;
import net.project.uncdf.edun.databaseacces.dao.ThemeDao;
import net.project.uncdf.edun.databaseacces.dao.VideoDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig parametreDaoConfig;
    private final DaoConfig partenaireDaoConfig;
    private final DaoConfig themeDaoConfig;
    private final DaoConfig videoDaoConfig;

    private final ParametreDao parametreDao;
    private final PartenaireDao partenaireDao;
    private final ThemeDao themeDao;
    private final VideoDao videoDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        parametreDaoConfig = daoConfigMap.get(ParametreDao.class).clone();
        parametreDaoConfig.initIdentityScope(type);

        partenaireDaoConfig = daoConfigMap.get(PartenaireDao.class).clone();
        partenaireDaoConfig.initIdentityScope(type);

        themeDaoConfig = daoConfigMap.get(ThemeDao.class).clone();
        themeDaoConfig.initIdentityScope(type);

        videoDaoConfig = daoConfigMap.get(VideoDao.class).clone();
        videoDaoConfig.initIdentityScope(type);

        parametreDao = new ParametreDao(parametreDaoConfig, this);
        partenaireDao = new PartenaireDao(partenaireDaoConfig, this);
        themeDao = new ThemeDao(themeDaoConfig, this);
        videoDao = new VideoDao(videoDaoConfig, this);

        registerDao(Parametre.class, parametreDao);
        registerDao(Partenaire.class, partenaireDao);
        registerDao(Theme.class, themeDao);
        registerDao(Video.class, videoDao);
    }
    
    public void clear() {
        parametreDaoConfig.getIdentityScope().clear();
        partenaireDaoConfig.getIdentityScope().clear();
        themeDaoConfig.getIdentityScope().clear();
        videoDaoConfig.getIdentityScope().clear();
    }

    public ParametreDao getParametreDao() {
        return parametreDao;
    }

    public PartenaireDao getPartenaireDao() {
        return partenaireDao;
    }

    public ThemeDao getThemeDao() {
        return themeDao;
    }

    public VideoDao getVideoDao() {
        return videoDao;
    }

}
