package net.project.uncdf.edun.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.adapter.PartenaireListAdapter;
import net.project.uncdf.edun.adapter.PartenaireListFiltreAdapter;
import net.project.uncdf.edun.adapter.ThemeListAdapter;
import net.project.uncdf.edun.adapter.ThemeListFiltreAdapter;
import net.project.uncdf.edun.adapter.VideoListAdapter;
import net.project.uncdf.edun.databaseacces.dao.VideoDao;
import net.project.uncdf.edun.databaseacces.entities.Partenaire;
import net.project.uncdf.edun.databaseacces.entities.Theme;
import net.project.uncdf.edun.databaseacces.entities.Video;
import net.project.uncdf.edun.utils.ConnexionUtil;
import net.project.uncdf.edun.utils.MyListView;

import java.util.List;

import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.partenaireDao;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.themeDao;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.videoDao;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment {

    List<Video> videoList;
    View view;
    public static ListView listView;
    AlertDialog dialog;


    public VideoFragment() {
        // Required empty public constructor
        setHasOptionsMenu(true);
    }
    public static VideoFragment newInstance() {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConnexionUtil.dbConnexion(getContext());

        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_video, container, false);
        listView = view.findViewById(R.id.list_data);
        dataBindListVideo();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.video_fragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_filtre:
                showFiltre();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void dataBindListVideo(){
        try {
            videoList = videoDao.loadAll();
            VideoListAdapter adapter = new VideoListAdapter(getContext(),videoList);
            listView.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showFiltre() {

        try {
            List<Partenaire> partenaireList = partenaireDao.loadAll();
            List<Theme> themeList = themeDao.loadAll();
            PartenaireListFiltreAdapter adapterP = new PartenaireListFiltreAdapter(getContext(), partenaireList);
            ThemeListFiltreAdapter adapterT = new ThemeListFiltreAdapter(getContext(), themeList);

            final AlertDialog.Builder box = new AlertDialog.Builder(getContext());

            LinearLayout ll = (LinearLayout) this.getLayoutInflater().inflate(R.layout.dialog_filtre_video, null);
            box.setView(ll);

            TextView bt_voir_tout = ll.findViewById(R.id.bt_voir_tout);
            bt_voir_tout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    dataBindListVideo();
                }
            });

            MyListView listViewP = ll.findViewById(R.id.list_partenaire);

            listViewP.setAdapter(adapterP);
            listViewP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Partenaire p = (Partenaire) parent.getAdapter().getItem(position);

                    dialog.dismiss();
                    filterVideoByPartenaire(p.getId());
                }
            });

            MyListView listViewT = ll.findViewById(R.id.list_theme);
            listViewT.setAdapter(adapterT);
            listViewT.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Theme t = (Theme) parent.getAdapter().getItem(position);

                    dialog.dismiss();
                    filterVideoByTheme(t.getId());
                }
            });

            dialog = box.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void filterVideoByPartenaire(Long idPartenaire){
        try {
            videoList = videoDao.queryBuilder().where(VideoDao.Properties.IdPartenaire.eq(idPartenaire)).list();
            VideoListAdapter adapter = new VideoListAdapter(getContext(),videoList);
            listView.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void filterVideoByTheme(Long idTheme){
        try {
            videoList = videoDao.queryBuilder().where(VideoDao.Properties.IdTheme.eq(idTheme)).list();
            VideoListAdapter adapter = new VideoListAdapter(getContext(),videoList);
            listView.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
