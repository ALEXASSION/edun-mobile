package net.project.uncdf.edun.adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.project.uncdf.edun.databaseacces.entities.Partenaire;

import java.util.List;

import net.project.uncdf.edun.R;

public class PartenaireListAdapter extends BaseAdapter implements ListAdapter{

    List<Partenaire> partenaires;
    LayoutInflater myInflater;
    Context context;

    public PartenaireListAdapter(Context context, List<Partenaire> partenaires) {
        this.partenaires = partenaires;
        this.myInflater = LayoutInflater.from(context);
        this.context =context;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return partenaires.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return partenaires.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public static class viewCarnetHolder {
        TextView tx_nom,tx_titre,tx_infos;
        ImageView logo;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewCarnetHolder holder;
        if (convertView==null) {
            convertView = myInflater.inflate(R.layout.item_partenaire, null);
            holder = new viewCarnetHolder();
            holder.tx_nom = (TextView)convertView.findViewById(R.id.tx_nom_partenaire);
            holder.tx_infos = (TextView)convertView.findViewById(R.id.tx_infos_partenaire);
            holder.logo = (ImageView)convertView.findViewById(R.id.logo_ly);


            convertView.setTag(holder);
        } else {
            holder = (viewCarnetHolder)convertView.getTag();
        }
        holder.tx_nom.setText(partenaires.get(position).getNompartenaire());
        holder.tx_infos.setText(partenaires.get(position).getInfospartenaire());

        if(partenaires.get(position).getNompartenaire().toUpperCase().equals("UNCDF")){
            holder.logo.setImageDrawable(context.getResources().getDrawable(R.drawable.uncdf));
        }else if(partenaires.get(position).getNompartenaire().toUpperCase().equals("UNFPA")){
            holder.logo.setImageDrawable(context.getResources().getDrawable(R.drawable.unfpa));
        }else if(partenaires.get(position).getNompartenaire().toUpperCase().equals("TREE AID")){
            holder.logo.setImageDrawable(context.getResources().getDrawable(R.drawable.treeaid));
        }else if(partenaires.get(position).getNompartenaire().toUpperCase().equals("PARI")){
            holder.logo.setImageDrawable(context.getResources().getDrawable(R.drawable.pari));
        }else if(partenaires.get(position).getNompartenaire().toUpperCase().equals("LUXEMBOURG")){
            holder.logo.setImageDrawable(context.getResources().getDrawable(R.drawable.luxembourg));
        }else if(partenaires.get(position).getNompartenaire().toUpperCase().equals("SUEDE")){
            holder.logo.setImageDrawable(context.getResources().getDrawable(R.drawable.suede));
        }else if(partenaires.get(position).getNompartenaire().toUpperCase().equals("BELGIQUE")){
            holder.logo.setImageDrawable(context.getResources().getDrawable(R.drawable.belgique));
        }else {
            holder.logo.setImageDrawable(null);
        }


        return convertView;
    }


}
