package net.project.uncdf.edun.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.project.uncdf.edun.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class JeuxFragment extends Fragment {


    public JeuxFragment() {
        // Required empty public constructor
    }


    public static JeuxFragment newInstance() {
        JeuxFragment fragment = new JeuxFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_jeux, container, false);
    }

}
