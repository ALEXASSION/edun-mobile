package net.project.uncdf.edun.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.adapter.PartenaireListAdapter;
import net.project.uncdf.edun.databaseacces.entities.Partenaire;
import net.project.uncdf.edun.utils.ConnexionUtil;
import net.project.uncdf.edun.utils.MyListView;

import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.partenaireDao;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class PartenaireFragment extends Fragment {

     List<Partenaire> partenaireList;
    View view;
    ListView listView;


    public PartenaireFragment() {
        // Required empty public constructor
    }

    public static PartenaireFragment newInstance() {
        PartenaireFragment fragment = new PartenaireFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConnexionUtil.dbConnexion(getContext());
        if (getArguments() != null) {

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         view = inflater.inflate(R.layout.fragment_partenaire, container, false);

        dataBindListPartenaire();

        return view;
    }

    public void dataBindListPartenaire(){
        try {
            partenaireList = partenaireDao.loadAll();
            PartenaireListAdapter adapter = new PartenaireListAdapter(getContext(),partenaireList);
            listView = view.findViewById(R.id.list_data);
            listView.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
