package net.project.uncdf.edun.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.utils.DatabaseUtil;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public static int idFragment=1;
    BottomNavigationView bottomNavigationView ;
    private FragmentManager fm;
    private Fragment active;
    private PartenaireFragment partenaireFragment;
    private VideoFragment videoFragment;
    private ThemeFragment themeFragment;
    private JeuxFragment jeuxFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new DatabaseUtil(this);
        setContentView(R.layout.activity_main);
        initView();
        initFragment();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void initFragment(){

        partenaireFragment = PartenaireFragment.newInstance();
        videoFragment = VideoFragment.newInstance();
        themeFragment = ThemeFragment.newInstance();
        jeuxFragment = JeuxFragment.newInstance();

        fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.fragment_layout, videoFragment,"1").commit();
        fm.beginTransaction().add(R.id.fragment_layout, partenaireFragment,"2").hide(partenaireFragment).commit();
        fm.beginTransaction().add(R.id.fragment_layout, themeFragment,"3").hide(themeFragment).commit();
        fm.beginTransaction().add(R.id.fragment_layout, jeuxFragment,"4").hide(jeuxFragment).commit();
        active = videoFragment;

    }

    private void initView(){
        toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);


        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_video:
                                showFragment(1);
                                break;
                            case R.id.action_partenaire:
                                showFragment(2);
                                break;
                           /*case R.id.action_theme:
                                showFragment(3);
                                break;*/
                            case R.id.action_jeux:
                                showFragment(4);
                                break;
                        }
                        return true;
                    }
                });


    }


    private void showFragment(int id){

        if (id == 1) {
            showVideo();
            idFragment = 1;
        }
        if (id == 2) {
            showPartenaire();
            idFragment = 2;

        }

        if (id == 3) {
            showTheme();
            idFragment = 3;
        }

        if (id == 4) {
            showJeux();
            idFragment = 4;
        }



    }

    private void showPartenaire(){
        getSupportActionBar().setTitle(R.string.partenaire);
        fm.beginTransaction().hide(active).show(partenaireFragment).commit();
        active = partenaireFragment;
    }

    private void showVideo(){
        getSupportActionBar().setTitle(R.string.videos);
        fm.beginTransaction().hide(active).show(videoFragment).commit();
        active = videoFragment;
    }

    private void showTheme(){
        getSupportActionBar().setTitle(R.string.theme_educatif);
        fm.beginTransaction().hide(active).show(themeFragment).commit();
        active = themeFragment;
    }

    private void showJeux(){
        getSupportActionBar().setTitle(R.string.jeux);
        fm.beginTransaction().hide(active).show(jeuxFragment).commit();
        active = jeuxFragment;
    }
}
