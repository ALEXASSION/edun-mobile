package net.project.uncdf.edun.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.databaseacces.entities.Partenaire;

import java.util.List;

public class PartenaireListFiltreAdapter extends BaseAdapter implements ListAdapter{

    List<Partenaire> partenaires;
    LayoutInflater myInflater;

    public PartenaireListFiltreAdapter(Context context, List<Partenaire> partenaires) {
        this.partenaires = partenaires;
        this.myInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return partenaires.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return partenaires.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public static class viewCarnetHolder {
        TextView tx_nom;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewCarnetHolder holder;
        if (convertView==null) {
            convertView = myInflater.inflate(R.layout.item_partenaire_filtre, null);
            holder = new viewCarnetHolder();
            holder.tx_nom = (TextView)convertView.findViewById(R.id.tx_nom_partenaire);


            convertView.setTag(holder);
        } else {
            holder = (viewCarnetHolder)convertView.getTag();
        }
        holder.tx_nom.setText(partenaires.get(position).getNompartenaire());


        return convertView;
    }


}
