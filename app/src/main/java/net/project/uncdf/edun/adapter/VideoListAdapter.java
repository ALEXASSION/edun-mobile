package net.project.uncdf.edun.adapter;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.activity.VideoActivity;
import net.project.uncdf.edun.databaseacces.dao.ThemeDao;
import net.project.uncdf.edun.databaseacces.dao.VideoDao;
import net.project.uncdf.edun.databaseacces.entities.Theme;
import net.project.uncdf.edun.databaseacces.entities.Video;
import net.project.uncdf.edun.utils.ConnexionUtil;
import net.project.uncdf.edun.utils.Constantes;
import net.project.uncdf.edun.utils.FileUtils;
import net.project.uncdf.edun.wsclient.WebserviceClient;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

import static net.project.uncdf.edun.activity.VideoFragment.listView;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.partenaireDao;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.themeDao;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.videoDao;

public class VideoListAdapter extends BaseAdapter implements ListAdapter {

    List<Video> videos;
    LayoutInflater myInflater;
    Context context;
    private final List<View> entryViews;
    private final Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;
    private final ThumbnailListener thumbnailListener;

    public VideoListAdapter(Context context, List<Video> videos) {
        this.videos = videos;
        this.context = context;
        this.myInflater = LayoutInflater.from(context);
        entryViews = new ArrayList<View>();
        thumbnailViewToLoaderMap = new HashMap<YouTubeThumbnailView, YouTubeThumbnailLoader>();
        thumbnailListener = new ThumbnailListener();

        ConnexionUtil.dbConnexion(context);
    }

    public void releaseLoaders() {
        for (YouTubeThumbnailLoader loader : thumbnailViewToLoaderMap.values()) {
            loader.release();
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return videos.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return videos.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public static class viewCarnetHolder {
        TextView tx_detail,tx_titre,bt_telecharger_video,tx_nom_partenaire,tx_nom_theme;
        VideoView videoView;
        YouTubeThumbnailView thumbnail;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VideoListAdapter.viewCarnetHolder holder;
        if (convertView==null) {
            convertView = myInflater.inflate(R.layout.item_video, null);

            holder = new VideoListAdapter.viewCarnetHolder();
            holder.tx_detail = (TextView)convertView.findViewById(R.id.tx_details_video);
            holder.tx_titre = (TextView)convertView.findViewById(R.id.tx_titre_video);
            holder.bt_telecharger_video = (TextView)convertView.findViewById(R.id.bt_telecharger_video);
            holder.tx_nom_partenaire = (TextView)convertView.findViewById(R.id.tx_nom_partenaire);
            holder.tx_nom_theme = (TextView)convertView.findViewById(R.id.tx_nom_theme);
            //holder.videoView = (VideoView)convertView.findViewById(R.id.video_thumb);
            holder.thumbnail = (YouTubeThumbnailView) convertView.findViewById(R.id.video_img);

            holder.thumbnail.setTag(videos.get(position).getChemeinvideo());
            holder.thumbnail.initialize(VideoActivity.YoutubeDeveloperKey, thumbnailListener);

            convertView.setTag(holder);
        } else {

            holder = (VideoListAdapter.viewCarnetHolder)convertView.getTag();

            YouTubeThumbnailLoader loader = thumbnailViewToLoaderMap.get(holder.thumbnail);
            if (loader == null) {
                // 2) The view is already created, and is currently being initialized. We store the
                //    current videoId in the tag.
                holder.thumbnail.setTag(videos.get(position).getChemeinvideo());
            } else {
                // 3) The view is already created and already initialized. Simply set the right videoId
                //    on the loader.
                holder.thumbnail.setImageResource(R.drawable.loading_thumbnail);
                loader.setVideo(videos.get(position).getChemeinvideo());
            }
        }
        holder.tx_detail.setText(videos.get(position).getDetailsvideo());
        holder.tx_titre.setText(videos.get(position).getTitrevideo());

        holder.tx_nom_theme.setText(themeDao.load(videos.get(position).getIdTheme()).getNomtheme());
        holder.tx_nom_partenaire.setText(partenaireDao.load(videos.get(position).getIdPartenaire()).getNompartenaire());

        filterVideoByPartenaire(holder.tx_nom_partenaire,videos.get(position).getIdPartenaire());
        filterVideoByTheme(holder.tx_nom_theme,videos.get(position).getIdTheme());

        openVideo(holder.thumbnail,videos.get(position).getId());
        downloadVideoWithSysManager(holder.bt_telecharger_video,videos.get(position));


        return convertView;
    }

    private void openVideo(YouTubeThumbnailView thumbnail,final long idvideo){
        thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,VideoActivity.class);
                intent.putExtra("id_video",idvideo);
                context.startActivity(intent);

            }
        });
    }


    public void filterVideoByPartenaire(TextView tx_partenaire,final Long idPartenaire){

        tx_partenaire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<Video> videoList = videoDao.queryBuilder().where(VideoDao.Properties.IdPartenaire.eq(idPartenaire)).list();
                    VideoListAdapter adapter = new VideoListAdapter(context,videoList);
                    listView.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void filterVideoByTheme(TextView tx_theme,final Long idTheme){
        tx_theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<Video> videoList = videoDao.queryBuilder().where(VideoDao.Properties.IdTheme.eq(idTheme)).list();
                    VideoListAdapter adapter = new VideoListAdapter(context,videoList);
                    listView.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }



    private final class ThumbnailListener implements
            YouTubeThumbnailView.OnInitializedListener,
            YouTubeThumbnailLoader.OnThumbnailLoadedListener {

        @Override
        public void onInitializationSuccess(
                YouTubeThumbnailView view, YouTubeThumbnailLoader loader) {
            loader.setOnThumbnailLoadedListener(this);
            thumbnailViewToLoaderMap.put(view, loader);
            view.setImageResource(R.drawable.loading_thumbnail);
            String videoId = (String) view.getTag();
            loader.setVideo(videoId);
        }

        @Override
        public void onInitializationFailure(
                YouTubeThumbnailView view, YouTubeInitializationResult loader) {
            view.setImageResource(R.drawable.no_thumbnail);
        }

        @Override
        public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
            //view.setMaxWidth(640);
            view.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        @Override
        public void onThumbnailError(YouTubeThumbnailView view, YouTubeThumbnailLoader.ErrorReason errorReason) {
            view.setImageResource(R.drawable.no_thumbnail);
        }
    }


    public void downloadVideoWithSysManager(TextView btview,final Video video){

        btview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    view.setEnabled(false);
                    view.setAlpha(0.4f);

                    Theme theme=themeDao.queryBuilder().where(ThemeDao.Properties.Id.eq(video.getIdTheme())).unique();

                    DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                    Uri Download_Uri = Uri.parse(Constantes.serveur.adresseServices+"/video?id="+video.getId()+"");

                    DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
                    request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
                    request.setAllowedOverRoaming(false);
                    request.setTitle("video" + video.getId() + ".mp4");
                    request.setDescription(video.getTitrevideo() + ".mp4");
                    //request.setVisibleInDownloadsUi(true);
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/Edfin/"+theme.getNomtheme()+"_" + video.getTitrevideo() + ".mp4");
                    Long refid = downloadManager.enqueue(request);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
