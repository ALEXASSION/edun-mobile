
package net.project.uncdf.edun.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import net.project.uncdf.edun.databaseacces.dao.DaoMaster;
import net.project.uncdf.edun.databaseacces.dao.DaoSession;
import net.project.uncdf.edun.databaseacces.dao.PartenaireDao;
import net.project.uncdf.edun.databaseacces.dao.ThemeDao;
import net.project.uncdf.edun.databaseacces.dao.VideoDao;



/**
 * Created by ASSION on 15/12/2015.
 */
public class ConnexionUtil {

    private static SQLiteDatabase db;
    private static DaoMaster daoMaster;
    private static DaoSession daoSession;
    private static String base = "edun-db";

    public static DaoSession dbConnexion(Context context){

        if(daoSession==null) {
            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, base, null);
            helper.close();
            db = helper.getWritableDatabase();
            DaoMaster.createAllTables(db,true);
        }
            daoMaster = new DaoMaster(db);
            daoSession = daoMaster.newSession();

        return daoSession;
    }

    public static class Daos{

        public static PartenaireDao partenaireDao = daoSession.getPartenaireDao();
        public static ThemeDao themeDao = daoSession.getThemeDao();
        public static VideoDao videoDao = daoSession.getVideoDao();


    }


}
