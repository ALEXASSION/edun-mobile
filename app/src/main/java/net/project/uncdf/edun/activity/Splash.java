package net.project.uncdf.edun.activity;

import android.content.Intent;
import android.print.PageRange;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.loopj.android.http.JsonHttpResponseHandler;

import net.project.uncdf.edun.R;
import net.project.uncdf.edun.databaseacces.dao.ThemeDao;
import net.project.uncdf.edun.databaseacces.entities.Partenaire;
import net.project.uncdf.edun.databaseacces.entities.Theme;
import net.project.uncdf.edun.databaseacces.entities.Video;
import net.project.uncdf.edun.utils.ConnexionUtil;
import net.project.uncdf.edun.utils.Constantes;
import net.project.uncdf.edun.wsclient.WebserviceClient;

import org.json.JSONArray;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.partenaireDao;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.themeDao;
import static net.project.uncdf.edun.utils.ConnexionUtil.Daos.videoDao;

public class Splash extends AppCompatActivity {

    private WebserviceClient webserviceClient = new WebserviceClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ConnexionUtil.dbConnexion(this);
        updateAppData();

    }


    Thread splashThread = new Thread() {
        @Override
        public void run() {
            try {

                int waited = 0;
                while (waited < 4100) {
                    sleep(50);
                    waited += 100;
                }
            } catch (InterruptedException e) {
                // do nothing
            } finally {


                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
                finish();

            }
        }
    };


    public void updateAppData(){
        updatePartenaires();
        updateThemes();
        updatevideos();

        splashThread.start();

    }

    private void updatePartenaires(){
        webserviceClient.getClient().get(Constantes.serveur.adresseServices+"/partenaires/", new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

                //System.err.println("Succes  "+response);

                if (response != null) {
                    partenaireDao.deleteAll();
                    for (int i = 0; i < response.length(); i++) {
                        Partenaire l = new Partenaire();
                        try {
                            JSONObject p = response.getJSONObject(i);
                            l.setId(p.getLong("id"));
                            l.setNompartenaire(p.getString("nomPartenaire"));
                            l.setInfospartenaire(p.getString("infosPartenaire"));
                            partenaireDao.insert(l);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                //super.onFailure(statusCode, headers, throwable, errorResponse);
                System.err.println("ERROR  "+errorResponse);
            }


        });

    }
    private void updateThemes(){
        webserviceClient.getClient().get(Constantes.serveur.adresseServices+"/themes/", new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

                //System.err.println("Succes  "+response);

                if (response != null) {
                    themeDao.deleteAll();
                    for (int i = 0; i < response.length(); i++) {
                        Theme l = new Theme();
                        try {
                            JSONObject p = response.getJSONObject(i);
                            l.setId(p.getLong("id"));
                            l.setIdPartenaire(p.getLong("idPartenaire"));
                            l.setNomtheme(p.getString("nomTheme"));
                            themeDao.insert(l);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                //super.onFailure(statusCode, headers, throwable, errorResponse);
                System.err.println("ERROR  "+errorResponse);
            }


        });

    }
    private void updatevideos(){
        webserviceClient.getClient().get(Constantes.serveur.adresseServices+"/videos/", new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {

                //System.err.println("Succes  "+response);

                if (response != null) {
                    videoDao.deleteAll();
                    for (int i = 0; i < response.length(); i++) {
                        Video l = new Video();
                        try {
                            JSONObject p = response.getJSONObject(i);
                            l.setId(p.getLong("id"));
                            l.setTitrevideo(p.getString("titreVideo"));
                            l.setChemeinvideo(p.getString("cheminVideo"));
                            l.setDatevideo(p.getString("dateVideo"));
                            l.setDetailsvideo(p.getString("detailsVideo"));
                            l.setIdTheme(p.getLong("idTheme"));
                            l.setTypevideo(p.getString("typeVideo"));
                            l.setLienlocalvideo(p.getString("lienLocalVideo"));

                            Long idPartenaire = themeDao.queryBuilder().where(ThemeDao.Properties.Id.eq(p.getLong("idTheme"))).unique().getIdPartenaire();
                            l.setIdPartenaire(idPartenaire);

                            videoDao.insert(l);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                //super.onFailure(statusCode, headers, throwable, errorResponse);
                System.err.println("ERROR  "+errorResponse);
            }


        });

    }


}
